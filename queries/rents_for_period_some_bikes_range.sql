\set vstart_datetime '''2020-07-01'''::date + interval '''09 hour'''
\set vend_datetime '''2020-07-01'''::date + interval '''20 hour'''

EXPLAIN (ANALYSE,BUFFERS)
SELECT
  *
  FROM rents_2_col r
  WHERE
  (
      (start_datetime BETWEEN :vstart_datetime AND  :vend_datetime)
  OR
     (end_datetime BETWEEN :vstart_datetime AND :vend_datetime)
  )
  AND r.bike_id < 100
;
