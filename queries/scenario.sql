
\set test_day_date '''2019-07-01'''::date


\timing
-- Modification de certaines locations pour les tests.
DELETE FROM rents WHERE rent_dates && TSTZRANGE(:test_day_date ,:test_day_date + interval '1 day') and bike_id = 9;
INSERT INTO rents (rent_dates,bike_id) VALUES (TSTZRANGE(:test_day_date + interval '08 hour',:test_day_date + interval '11 hour'), 9);
INSERT INTO rents (rent_dates,bike_id) VALUES (TSTZRANGE(:test_day_date + interval '16 hour',:test_day_date + interval '17 hour'), 9);
DELETE FROM rents WHERE rent_dates && TSTZRANGE(:test_day_date ,:test_day_date + interval '1 day') and bike_id = 800;
INSERT INTO rents (rent_dates,bike_id) VALUES (TSTZRANGE(:test_day_date + interval '11 hour',:test_day_date + interval '13 hour'), 800);


DELETE FROM rents_2_col WHERE start_datetime > :test_day_date AND end_datetime < :test_day_date + interval '1 day' and bike_id = 9;
INSERT INTO rents_2_col (start_datetime,end_datetime,bike_id) VALUES (:test_day_date + interval '08 hour',:test_day_date + interval '11 hour', 9);
INSERT INTO rents_2_col (start_datetime,end_datetime,bike_id) VALUES (:test_day_date + interval '16 hour',:test_day_date + interval '17 hour', 9);
DELETE FROM rents_2_col WHERE start_datetime > :test_day_date AND end_datetime < :test_day_date + interval '1 day' and bike_id = 800;
INSERT INTO rents_2_col (start_datetime,end_datetime,bike_id) VALUES (:test_day_date + interval '08 hour',:test_day_date + interval '11 hour', 800);


-- insertion de 10 vélos sans location...
with category_id_bike_id_gen as(
    select
    n as bike_name_suffix,
    get_random_category_id() as id
    FROM generate_series(1,10) n
)
,bikes_generator_name as (
    select b_c.name ||'-'|| bike_name_suffix bike_name,
    id as category_id
    from
    category_id_bike_id_gen
    join bike_categories b_c using(id)
)
INSERT INTO bikes(name,category_id) SELECT bike_name,category_id from bikes_generator_name;
