\set search_range TSTZRANGE('''2020-07-01'''::date + interval '''01 hour''' ,'''2020-07-01'''::date + interval ''' 20 hour''')
EXPLAIN (ANALYSE, buffers)
with available_rents_range  AS (
    SELECT bike_id
        ,TSTZRANGE (
            lag (upper(rent_dates)) OVER (partition by bike_id ORDER BY (rent_dates))
            ,lower(rent_dates)
        ) AS available_range_left
        ,TSTZRANGE (
            upper(rent_dates)
            ,Lead (lower(rent_dates)) OVER (partition by bike_id ORDER BY (rent_dates))
        )AS available_range_right
    FROM rents
    WHERE rent_dates && :search_range
    ORDER BY rent_dates, bike_id
)
,unnest_available_rents_bikes_range AS (
    SELECT
        unnest(array[available_range_left,available_range_right])::TSTZRANGE AS available_range
        ,bike_id
    FROM available_rents_range
)
SELECT
    bike_id
    ,available.available_range AS available_range
FROM unnest_available_rents_bikes_range available
WHERE
    isempty(available_range) = FALSE

UNION
    SELECT id AS bike_id
    ,:search_range AS available_range
    FROM bikes
    WHERE NOT EXISTS (
        SELECT bike_id
        FROM   rents
        WHERE bike_id = bikes.id
        AND rent_dates && :search_range
    )
GROUP BY bike_id,available_range
ORDER BY available_range;

