\set search_range TSTZRANGE('''2020-07-01'''::date + interval '''09 hour''' ,'''2020-07-01'''::date + interval ''' 20 hour''')

EXPLAIN (ANALYSE,BUFFERS)
 SELECT * FROM rents r
 WHERE r.rent_dates && :search_range AND r.bike_id < 100;