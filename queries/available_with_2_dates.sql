--SET search_path = ams_rent_bike;
\set vstart_datetime '''2020-07-01'''::date + interval '''01 hour'''
\set vend_datetime '''2020-07-01'''::date + interval '''20 hour'''

EXPLAIN (ANALYSE, buffers)
with previous_next_date AS (
SELECT
    bike_id
    ,COALESCE (
        lag (end_datetime)
        OVER (
            PARTITION by bike_id
            ORDER BY end_datetime
    ), '-infinity'::date) AS previous_end_date
    ,start_datetime
    ,end_datetime
    ,COALESCE (lead (start_datetime)
    OVER (
        PARTITION by bike_id
        ORDER BY start_datetime
    ),'infinity'::date) next_start_datetime
    FROM rents_2_col
    WHERE
        (start_datetime BETWEEN :vstart_datetime AND  :vend_datetime)
        OR
        (end_datetime BETWEEN :vstart_datetime AND :vend_datetime)

    ), available_periods as (
SELECT
    bike_id,
    unnest(
        array[previous_end_date ,
        end_datetime ]
    ) AS start_datetime,
    unnest(
        array[start_datetime,
        next_start_datetime ]
    ) AS end_datetime
  FROM previous_next_date
  WHERE
    previous_end_date <> start_datetime or end_datetime <> next_start_datetime
)
SELECT bike_id, start_datetime,  end_datetime
FROM available_periods
where start_datetime <> end_datetime
UNION
    SELECT id as bike_id,
    :vstart_datetime AS start_datetime,
    :vend_datetime AS end_datetime
    FROM bikes
WHERE NOT EXISTS (
    SELECT bike_id
    FROM
        rents_2_col
    WHERE bike_id = bikes.id
    AND
    ((start_datetime >= :vstart_datetime AND start_datetime <= :vend_datetime)
    OR
    (end_datetime > :vstart_datetime AND end_datetime < :vend_datetime))
)
GROUP BY bike_id,start_datetime, end_datetime
ORDER BY start_datetime
