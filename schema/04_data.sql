
\set insert_start_period '''2019-01-01'''::date
\set insert_end_period '''2020-12-30'''::date
\set test_day_date '''2020-07-01'''::date

INSERT INTO bike_categories (name)
VALUES('Single-Speed'),
    ('Cargo'),
    ('Gravel'),
    ('City'),
    ('Montain bike'),
    ('Road');

CREATE OR REPLACE FUNCTION get_random_category_id()
   RETURNS INT AS 'SELECT id FROM bike_categories order by random() LIMIT 1'
   language 'sql' VOLATILE;

-- Création de 1000 vélos associés de façon aléatoire à une catégorie
with category_id_bike_id_gen as(
    select
    n as bike_name_suffix,
    get_random_category_id() as id
    FROM generate_series(1,1000) n
)
,bikes_generator_name as (
    select b_c.name ||'-'|| bike_name_suffix bike_name,
    id as category_id
    from
    category_id_bike_id_gen
    join bike_categories b_c using(id)
)
INSERT INTO bikes(name,category_id) SELECT bike_name,category_id from bikes_generator_name;

--Création de 3 locations / jour pour les 1000 vélos sur une période allant du 01-01-2019 au 31-12-2020
\timing
\echo 'INSERT INTO rents';
with static_range_location_hour AS (
    SELECT unnest(
        array  [ interval '8 hour',
                interval '10 hour',
                interval '14 hour'
        ]
    ) AS start_hour
    ,unnest(
        array  [ interval '10 hour',
                interval '14 hour',
                interval '18 hour'
        ]
    ) AS end_hour
)
INSERT INTO rents (rent_dates, bike_id)
SELECT TSTZRANGE (day + range_hour.start_hour, day + range_hour.end_hour)
    ,bikes.id
    FROM generate_series(:insert_start_period, :insert_end_period, '1 day') day
    join static_range_location_hour AS range_hour ON true
    join bikes ON true
;
\timing
-- Modification de certaines locations pour les tests.
DELETE FROM rents WHERE rent_dates && TSTZRANGE(:test_day_date ,:test_day_date + interval '1 day') and bike_id = 9;
INSERT INTO rents (rent_dates,bike_id) VALUES (TSTZRANGE(:test_day_date + interval '08 hour',:test_day_date + interval '11 hour'), 9);
INSERT INTO rents (rent_dates,bike_id) VALUES (TSTZRANGE(:test_day_date + interval '16 hour',:test_day_date + interval '17 hour'), 9);
DELETE FROM rents WHERE rent_dates && TSTZRANGE(:test_day_date ,:test_day_date + interval '1 day') and bike_id = 800;
INSERT INTO rents (rent_dates,bike_id) VALUES (TSTZRANGE(:test_day_date + interval '11 hour',:test_day_date + interval '13 hour'), 800);

-- Insertion du même type de jeu de données dans la table rents_2_dates
\timing
\echo 'INSERT INTO rents_2_dates';
with static_range_location_hour AS (
    SELECT unnest(
        array  [ interval '8 hour',
                interval '10 hour',
                interval '14 hour'
        ]
    ) AS start_hour
    ,unnest(
        array  [ interval '10 hour',
                interval '14 hour',
                interval '18 hour'
        ]
    ) AS end_hour
)
INSERT INTO rents_2_dates (
    start_datetime
    ,end_datetime
    , bike_id
)
SELECT day + range_hour.start_hour
    , day + range_hour.end_hour
    ,bikes.id
    FROM generate_series(:insert_start_period, :insert_end_period, '1 day') day
    join static_range_location_hour AS range_hour ON true
    join bikes ON true
;
\timing

DELETE FROM rents_2_dates WHERE start_datetime > :test_day_date AND end_datetime < :test_day_date + interval '1 day' and bike_id = 9;
INSERT INTO rents_2_dates (start_datetime,end_datetime,bike_id) VALUES (:test_day_date + interval '08 hour',:test_day_date + interval '11 hour', 9);
INSERT INTO rents_2_dates (start_datetime,end_datetime,bike_id) VALUES (:test_day_date + interval '16 hour',:test_day_date + interval '17 hour', 9);
DELETE FROM rents_2_dates WHERE start_datetime > :test_day_date AND end_datetime < :test_day_date + interval '1 day' and bike_id = 800;
INSERT INTO rents_2_dates (start_datetime,end_datetime,bike_id) VALUES (:test_day_date + interval '08 hour',:test_day_date + interval '11 hour', 800);


-- insertion de 10 vélos sans location...
with category_id_bike_id_gen as(
    select
    n as bike_name_suffix,
    get_random_category_id() as id
    FROM generate_series(1,10) n
)
,bikes_generator_name as (
    select b_c.name ||'-'|| bike_name_suffix bike_name,
    id as category_id
    from
    category_id_bike_id_gen
    join bike_categories b_c using(id)
)
INSERT INTO bikes(name,category_id) SELECT bike_name,category_id from bikes_generator_name;
