\c postgres
DROP DATABASE IF EXISTS bike_location;
CREATE DATABASE bike_location;

\c bike_location
CREATE EXTENSION pg_stat_statements;

BEGIN;
\i 02_schema.sql
\i 03_trigger.sql
set track_functions = 'pl';
\i 04_data.sql

COMMIT;
