CREATE OR REPLACE FUNCTION check_rent_datetime_validity()  RETURNS TRIGGER AS
$rents_2_dates$
DECLARE counting_of_existing_rentals TEXT;
DECLARE number_of_rents INTEGER;
BEGIN
    counting_of_existing_rentals := '
      SELECT COUNT(*) AS count_rent
        FROM rents_2_dates
        WHERE ((start_datetime >= $1 AND start_datetime <= $2  )
                OR (end_datetime > $1 AND end_datetime < $2  ) )
        AND bike_id = $3 ';
    -- la date de début d'une location ne doit pas être entre la date de début et de fin
    -- d'une nouvelle entrée. Idem pour la date de fin.
    IF (TG_OP = 'UPDATE') THEN
        -- même cas de figure que pour l'insert, mais on exclut la location à mettre à jour.
        EXECUTE counting_of_existing_rentals || ' AND id <> $4'
        INTO number_of_rents
        USING
        new.start_datetime
        ,new.end_datetime
        ,new.bike_id
        ,new.id;
    ELSIF (TG_OP = 'INSERT') THEN
        EXECUTE counting_of_existing_rentals
        INTO number_of_rents
        USING
        new.start_datetime
        ,new.end_datetime
        ,new.bike_id;
    END IF;
    IF number_of_rents > 0 THEN
        RAISE EXCEPTION 'Cette plage horaire n''est pas disponible pour bike_id % pour la période % - % ', new.bike_id, new.start_datetime, new.end_datetime ;
        RETURN NULL;
    END IF;
    RETURN NEW;
END;
$rents_2_dates$ LANGUAGE plpgsql;

CREATE TRIGGER trg_check_rent_datetime_validity
  BEFORE INSERT OR UPDATE
  ON rents_2_dates
  FOR EACH ROW
  EXECUTE PROCEDURE check_rent_datetime_validity();
