# Introduction
Voici l'ensemble des fichiers sql qui ont servi à illustré l'article [Type range](https://www.loxodata.com/post/type-range-2/)


Dans le répertoire `queries` vous trouverez 2 fichiers

 * `available_with_2_dates.sql` sortant les disponibilités pour 1 journée de la table `rents_2_dates`
 * `available_with_range.sql` sortant les disponibilités pour 1 journée de la table `rents`
 * `rents_for_period_some_bikes_2_dates.sql` sortant les locations sur une periode données pour une liste de vélo de la table `rents_2_dates`
 * `rents_for_period_some_bikes_range` sortant les locations sur une periode données pour une liste de vélo de la table `rents`


Dans le répertoire `analyse` vous trouverez un fichier sortant la taille des tables et des indexes pour le schéma de l'exemple.

Le répertoire `explain_analyse` contient les plan d'exécutions utilisés dans l'article issues des requêtes du répertoire `queries`.

# Installation


L'installation se décompose en 4 fichiers à 


Le fichier `01_init.sql` se charge de se connecter à la base de données postgres, crée la base de données `bike_location`, 
se connecte à cette nouvelle base, crée l'extension `pg_stat_statements` puis appelle successivement `02_schema.sql`, `03_trigger.sql` et `04_data.sql`.


Le fichier `02_schema.sql` crée le schéma et les tables, `03_trigger.sql`,  crée les fonctions et les déclencheurs de contrôle et 
`04_data.sql`  insère un jeu de données.

```shell
    psql -U postgres -h <hostname> -p <pg_port> -f 01_init.sql
    DROP DATABASE
    CREATE DATABASE
    SSL connection (protocol: TLSv1.2, cipher: ECDHE-RSA-AES256-GCM-SHA384, bits: 256, compression: off)
    You are now connected to database "location" as user "postgres".
    CREATE SCHEMA
    SET
    psql:schema.sql:4: NOTICE:  table "bikes" does not exist, skipping
    psql:schema.sql:4: NOTICE:  table "bike_categories" does not exist, skipping
    psql:schema.sql:4: NOTICE:  table "rents" does not exist, skipping
    psql:schema.sql:4: NOTICE:  table "rents_2_dates" does not exist, skipping
    DROP TABLE
    CREATE TABLE
    CREATE TABLE
    CREATE EXTENSION
    CREATE TABLE
    CREATE TABLE
    CREATE INDEX
    CREATE INDEX
    CREATE FUNCTION
    .....
```
